<?php
    session_start();
    if ( !isset($_SESSION['is_connected']) || $_SESSION['is_connected'] != 'oui' || !isset($_SESSION['ID']) || !isset($_SESSION['Pseudo']) ){
        $_SESSION['error_msg'] = "Vous n'êtes pas connecté à votre compte.<br>Veuillez vous connecter.";
        header("Location: login.php");
        exit;
    }
    unset($_SESSION['ID_receiver']);
    unset($_SESSION['Pseudo_receiver']);
    $ID = $_SESSION['ID'];
    $Pseudo = htmlspecialchars($_SESSION['Pseudo']); //htmlspecialchars : allow special characters as '<'
    date_default_timezone_set("Europe/Paris");
    //echo date("H:i:s d/m/Y");
?>


<?php
    $servername = "localhost";
    $login = "root";
    $pass = "";

    //server connexion test
    try{
        $connexion = new PDO("mysql:host=$servername;dbname=cy_love_database", $login, $pass);
        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //PDO error mode

        /*  Récupère tous les IDs des personnes qui ont envoyé
            ou reçu de l'utilisateur un message dans l'ordre
            anti-chronologique (avec ID_msg DESC)*/
        $query = $connexion->prepare("
                SELECT * FROM messages
                WHERE (ID_user_sending = :ID) OR (ID_user_receiving = :ID)
                ORDER BY ID_msg DESC");
        $query->bindParam(':ID', $ID);
        $query->execute();
        $history_all_messages = $query->fetchall(PDO::FETCH_ASSOC); //associative array with infos of all messages exchanged with user ID (sorted by ID DESC)

        // Garde seulement le dernier message échangé avec chaque utilisateur
        $last_message = array();
        for($i=0; $i<count($history_all_messages); $i++){
            if( $history_all_messages[$i]['ID_user_sending'] == $ID ){
                $contact_ID = $history_all_messages[$i]['ID_user_receiving']; //potentiel nouveau contact à insérer
            }
            elseif( $history_all_messages[$i]['ID_user_receiving'] == $ID ){
                $contact_ID = $history_all_messages[$i]['ID_user_sending'];
            }
            else{
                $_SESSION['error_msg'] = "Impossible de charger les messages.<br>ERREUR n°1 : des messages ne vous incluant pas ont été récupérés.";
                header("Location: personal-acount.php");
                exit;
            }
            //vérifie si l'ID du contact est déjà dans $contact_ID.
            //S'il y est, on a déjà un message plus récent échangé entre ID et le contact.
            $push = 1;
            for($j=0; $j<count($last_message); $j++){
                $push = 1;
                if( $contact_ID == $last_message[$j]['ID_user_sending'] || $contact_ID == $last_message[$j]['ID_user_receiving'] ){
                    $push = 0;
                    break;
                }
            }

            if($push == 1){
                array_push($last_message, $history_all_messages[$i]);
            }
        }
    }
    catch (PDOException $e){
        echo "Connexion impossible à la base de données: " . $e->getMessage();
        exit;
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style2.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>CY LOVE</title>
    <style>
        .form-box {
            max-width: 600px;
            margin-top: 100px; /* 70px du nav + 30px margin*/
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
            text-align: center;
        }
        .contact-box{
            display: block;
            margin-top: 100px; /* 70px du nav + 30px margin*/
            width: 640px;
            padding: 20px;
            background: rgba(0, 0, 0, 0.6);
            border-radius: 10px;
        }
        .history_contacts{
            display: block;
            width: 100%;
            min-height: 300px;
            background: rgba(255, 255, 255, 0.2);
            border-radius: 10px;
        }
        .chat_contact{
            display: block;
            height: 120px;
            font-size: 16px;
            background: rgba(255, 255, 255, 0.2);
            border-radius: 10px;
            margin-bottom: 10px;
            overflow: hidden; /*cache le texte qui dépasse de la zone*/
            cursor: pointer;
            border: 2px solid black;
        }
        .chat_contact:hover{
            background: rgb(255, 255, 255);
        }
        .chat_contact .info_contact{
            display: flex;
            flex-direction : row;
            justify-content: space-between;
            height: 60%;
            padding: 5px;
            border-radius: 10px 10px 0px 0px;
            border-bottom: 1px solid rgba(0, 0, 0, 0.6);
        }

        .chat_contact .last_message{
            display: flex;
            flex-direction : row;
            justify-content: space-between;
            height: 40%;
            padding: 5px;
            overflow: hidden;
        }

        .chat_contact .info_contact span{
            height: 100%;
            font-size: 16px;
            line-height: 100%;
        }
        .chat_contact .last_message span{
            height: 100%;
            font-size: 16px;
            line-height: 100%;
        }

        .chat_contact .info_contact .profile_picture_contact{
            display: flex;
            width: 20%;
        }
        .chat_contact .info_contact .pseudo{
            display: flex;
            align-items: center;
            font-size: 20px;
            font-weight: bold;
        }
        .chat_contact .info_contact .time{
            display: flex;
            justify-content: flex-end; /* to align text on the right */
            text-align: right;
            align-items: end;
            width: 15%;
            font-size: 12px;
            color: rgba(0, 0, 0, 0.7);
        }

        .chat_contact .last_message .sender{
            display: flex;
            width: 20%;
            justify-content: flex;
            justify-content: flex-end; /* to align text on the right */
            text-align: right;
            align-items: start;
            font-style: italic;
            color: rgba(0, 0, 0, 0.7);
        }
        .chat_contact .last_message .message{
            display: flex;
            width: 70%;
            align-items: start;
            overflow: hidden; /*cache le texte qui dépasse de la zone*/
            color: rgba(0, 0, 0, 0.7);
        }




        .new-contact-container{
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;
            margin-right: auto;
            margin-left: auto;
            text-align: center;
            width: 100%;
        }

        .input-field-message {
            font-size: 15px;
            background: rgba(255, 255, 255, 0.2);
            color: #fff;
            height: 50px;
            width: 100%;
            padding: 0 20px;
            padding-left: 45px;
            border: none;
            border-radius: 30px;
            outline: none;
            transition: .2s ease;
        }

        .input-field-message:hover, .input-field-message:focus {
            background: rgba(255, 255, 255, 0.25);
        }

        .submit-message {
            font-size: 15px;
            font-weight: 500;
            color: black;
            height: 45px;
            width: 30%;
            border: none;
            border-radius: 30px;
            outline: none;
            background: rgba(255, 255, 255, 0.7);
            cursor: pointer;
            transition: .3s ease-in-out;
        }

        .submit-message:hover {
            background: rgba(255, 255, 255, 0.5);
            box-shadow: 1px 5px 7px 1px rgba(0, 0, 0, 0.2);
        }
    </style>
</head>

<body style="background-image: url('Images/Background_images.jpg')"> <!--Background image : https://img.freepik.com/photos-gratuite/jeune-couple-romantique-sexy-amoureux-heureux-plage-ete-ensemble-s-amusant-portant-maillots-bain-montrant-signe-du-coeur-sundet_285396-6545.jpg?t=st=1715103572~exp=1715107172~hmac=144c7e5b0ff875c6caeab703b9f2860b0da711ca04f6eb9e9186eb8b7e9f819d&w=2000-->
    <div class="wrapper">
        <?php include 'header.php'?>
        <?php include 'account_icon_bar.php'?>

        <div class="contact-box">
            <!-- Search a new contact -->
            <div class="new-contact-container">
                <form action="chat.php" method="post" enctype="multipart/form-data">
                    <div class="top">
                        <?php
                        if(isset($_SESSION['error_msg'])){
                            echo "<div style=\"color: rgb(255, 50, 50)\">" . htmlspecialchars($_SESSION['error_msg']) . "</div>";
                        }
                        unset($_SESSION['error_msg']);
                        ?>
                    </div>
                    <div class="input-box">
                        <label for="Pseudo_receiver">Nouveau destinataire</label>
                        <input type="text" name="Pseudo_receiver" class="input-field-message" placeholder="Pseudo du destinataire" required>
                        <i class="bx bx-user"></i>
                    </div>
                    <div class="input-box">
                        <input type="submit" name="submit" class="submit-message" value="Rechercher">
                    </div>
                </form>
            </div>

            <!-- Show old discussions with known contacts -->
            <div class="history_contacts">
                <?php
                    if(count($last_message)==0){
                        echo "Vous n'avez eu aucune discussion.";
                    }
                    for($i=0; $i<count($last_message) ;$i++){
                        //Récupère infos dans variables : $ID_contact, $pseudo_contact, $image_contact, $msg
                        if( $last_message[$i]['ID_user_sending'] == $ID ){
                            $ID_contact = $last_message[$i]['ID_user_receiving'];
                            $sender = "Moi";
                        }
                        elseif( $last_message[$i]['ID_user_receiving'] == $ID ){
                            $ID_contact = $last_message[$i]['ID_user_sending'];
                            $sender = "Reçu";
                        }
                        else{
                            $_SESSION['error_msg'] = "Impossible de charger les messages.<br>ERREUR n°2 : des messages ne vous incluant pas ont été récupérés.";
                            header("Location: personal-acount.php");
                            exit;
                        }
                        $query_pseudo_contact = $connexion->prepare("
                                SELECT Pseudo FROM user_info
                                WHERE ID = :ID");
                        $query_pseudo_contact->bindParam(':ID', $ID_contact);
                        $query_pseudo_contact->execute();
                        $pseudo_contact = $query_pseudo_contact->fetchColumn(); //récupère seulement la valeur, pas un tableau

                        $image_contact = "Accounts/ID_" . $ID_contact . "/profile_picture/profile_picture_ID_" . $ID_contact . ".jpg";
                        $msg = $last_message[$i]['Message'];
                        $date = $last_message[$i]['Date'];

                        echo "<div class=\"chat_contact\" data-id=\"" . $ID_contact . "\" >
                                <div class=\"info_contact\">
                                    <span class=\"profile_picture_contact\">";
                                    if(file_exists($image_contact)){
                                        echo "<img src=\"" . $image_contact . "\" alt=\"photo\" style=\"margin-right: auto; margin-left: auto;\">";
                                    }
                                    echo "</span>
                                    <span class=\"pseudo\">" . htmlspecialchars_decode($pseudo_contact) . "</span>
                                    <span class=\"time\">" . $date ."</span>
                                </div>
                                <div class=\"last_message\">
                                    <span class=\"sender\">" . htmlspecialchars_decode($sender) . " : </span>
                                    <span class=\"message\">" . htmlspecialchars_decode($msg) . "</span>
                                </div>
                            </div>";
                    }
                ?>
            <div>
        </div>
    </div>
    <script>
         $(document).ready(function() {
        $('.chat_contact').click(function() {   //get the click
            var contactID = $(this).data('id'); //get contactID
            $.ajax({                            //send contactID to store_contact_id.php via AJAX
                type: 'POST',
                url: 'store_contact_id.php',
                data: { contact_id: contactID },
                success: function(response) {
                    console.log(response);      //show response into the console
                    window.location.href = "chat.php";
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        });
    });
    </script>
</body>
</html>
