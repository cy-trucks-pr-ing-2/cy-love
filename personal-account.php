<?php
    session_start();
    if ( isset($_SESSION['is_connected']) && $_SESSION['is_connected'] == 'oui' && isset($_SESSION['ID']) && isset($_SESSION['Pseudo']) ){
        //session open
        //do nothing
    }
    else{
        $_SESSION['error_msg'] = "Vous n'êtes pas connecté à votre compte.<br>Veuillez vous connecter.";
        header("Location: login.php");
        exit;
    }
    $ID = $_SESSION['ID'];
    $Pseudo = $_SESSION['Pseudo'];
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CY LOVE - Modifier Informations</title>
    <link rel="stylesheet" href="style2.css">
</head>
<body style="background-image: url('Images/Background_images.jpg');">
    <div class="wrapper">
        <?php include 'header.php'?>
        <?php include 'account_icon_bar.php'?>
        <div class="form-box">
            <div class="login-container" id="modify-account">
                <?php
                    $servername = "localhost";
                    $login = "root";
                    $pass = "";

                    if( isset($_SESSION['Pseudo']) && isset($_SESSION['is_connected']) && $_SESSION['is_connected']=='oui' ){
                        //affiche les infos de compte
                    }
                    else{
                        echo "<p>Vous n'êtes pas connecté(e) à votre session.<br>
                            Connectez vous en cliquant <a href='login.php'>ici</a>.</p>";
                        exit;
                    }

                    //server connexion test
                    try{
                        $connexion = new PDO("mysql:host=$servername;dbname=cy_love_database", $login, $pass);
                        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //PDO error mode

                        $query_info_account = $connexion->prepare("SELECT * FROM user_info WHERE Pseudo = :pseudo");
                        $query_info_account->bindParam(':pseudo', $Pseudo, PDO::PARAM_STR);
                        $query_info_account->execute();
                        $array_info_account = $query_info_account->fetchAll(PDO::FETCH_NUM); // array with all info of the user
                        $ID = $array_info_account[0][0];
                        $_SESSION['ID'] = $ID;
                    }
                    catch (PDOException $e){
                        echo "Connexion impossible à la base de données: " . $e->getMessage();
                        exit;
                    }
                ?>
                <fieldset style="border: 0;">
                    <header>Vos informations <?php echo $_SESSION['Pseudo'];?></header>
                    <?php
                        if(isset($_SESSION['error_msg'])){
                            echo "<div style=\"color: rgb(255, 50, 50)\">" . $_SESSION['error_msg'] . "</div>";
                        }
                        unset($_SESSION['error_msg']); // remove only this session variable
                    ?>
                    <form action="register_modification_account.php" method="post" enctype="multipart/form-data"> <!-- enctype="multipart/form-data" allows the form to send file, can be used only if method="post"-->
                        <div class="input-box">
                            <label for="gender">Genre</label>
                            <input type="radio" name="gender" value="Madame">Madame
                            <input type="radio" name="gender" value="Monsieur">Monsieur
                            <input type="radio" name="gender" value="Non binaire">Non binaire
                            <input type="radio" name="gender" value="Non défini">Non défini
                        </div>
                        <div class="input-box">
                            <label for="Pseudo">Pseudo</label>
                            <input type="text" name="Pseudo" id="Pseudo" class="input-field" value="<?php echo $array_info_account[0][1]?>" >
                        </div>
                        <div class="input-box">
                            <label for="Password">Mot de passe</label>
                            <input type="text" name="Password" id="Password" class="input-field" value="<?php echo $array_info_account[0][2]?>" >
                        </div>
                        <div class="input-box">
                            <label for="Firstname">Prénom</label>
                            <input type="text" name="Firstname" id="Firstname" class="input-field" value="<?php echo $array_info_account[0][3]?>">
                        </div>
                        <div class="input-box">
                            <label for="Name">Nom</label>
                            <input type="text" name="Name" id="Name" class="input-field" value="<?php echo $array_info_account[0][4]?>">
                        </div>
                        <div class="input-box">
                            <label for="Email">Email</label>
                            <input type="text" name="Email" id="Email" class="input-field" value="<?php echo $array_info_account[0][5]?>">
                        </div>
                        <div class="input-box">
                            <label for="Profile_picture">Photo de profil</label>
                            <input type="file" name="Profile_picture" id="Profile_picture" accept="image/jpg">
                        </div>
                        <div class="input-box">
                            <label for="Profession">Profession</label>
                            <input type="text" name="Profession" id="Profession" class="input-field">
                        </div>
                        <div class="input-box">
                            <input type="submit" name="submit" class="submit" value="Enregistrer">
                        </div>
                    </form>
                </fieldset>
            </div>
        </div>
    </div>
</body>
</html>
