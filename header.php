<nav class="nav">
    <div class="nav-logo">
        <img src="Logos/Logo_CY_Love.svg" alt="Logo CY Love" onclick="document.location='login.php'">
    </div>
    <div class="nav-button">
        <?php
            if ( isset($_SESSION['is_connected']) && $_SESSION['is_connected'] == 'oui' && isset($_SESSION['ID']) && isset($_SESSION['Pseudo']) ){
                //session open
                echo "<button class=\"btn white-btn\" onclick=\"window.location.href='logout.php'\">Se déconnecter</button>";
            }
            else{
                echo "<button class=\"btn white-btn\" onclick=\"window.location.href='login.php'\">Se connecter</button>";
            }
        ?>
        <button class="btn" onclick="window.location.href='register.php'">S'inscrire</button>
    </div>
    <div class="nav-right" style="display: flex; justify-content: space-between; height: 100%; align-items: center;">
        <?php
            if ( isset($_SESSION['is_connected']) && $_SESSION['is_connected'] == 'oui' && isset($_SESSION['ID']) && isset($_SESSION['Pseudo']) ){
                //session open
                $ID = $_SESSION['ID'];
                echo "<div class=\"menu-profile-picture\">
                        <img src=\"Accounts/ID_" . $ID . "/profile_picture/profile_picture_ID_" . $ID . ".jpg\" onclick=\"document.location='personal-account.php'\">
                        </div>";
            }
        ?>
        <div class="menu-btn" id="menu-btn" onclick="myMenuFunction()">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div>
    </div>
    <div class="nav-menu" id="navMenu">
        <ul>
            <li><a href="#" class="link active">Menu Principal</a></li>
            <li><a href="personal-account.php" class="link">Compte</a></li>
            <li><a href="search.php" class="link">Rechercher un compte</a></li>
            <li><a href="service.php" class="link">Services</a></li>
            <li><a href="10profil.php" class="link">Voir les profils</a></li>
        </ul>
    </div>
</nav>
<script>
    function myMenuFunction() {
        var i = document.getElementById("menu-btn");
        var j = document.getElementById("navMenu");
        if(i.className === "menu-btn") {
            i.className += " responsive";
            j.className += " responsive";
        } else {
            i.className = "menu-btn";
            j.className = "nav-menu";
        }
    }
</script>
