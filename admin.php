<?php
// Start the session
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style2.css">
    <title>CY LOVE</title>
</head>
<body style="background-image: url('Images/Background_images.jpg')">
    <div class="wrapper">
        <?php include 'header.php'; ?>
          <?php include 'account_icon_bar.php'?>
        <h1>Liste de tous les profils</h1>
        <?php
        // Connexion à la base de données
        $servername = "localhost";
        $username = "root";
        $password = "";
        $database = "cy_love_database";

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Requête SQL pour récupérer les profils
            $sql = "SELECT ID, Pseudo, Prénom, nom, Photo_de_profil, email FROM user_info LIMIT 40";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            // Vérifier s'il y a des résultats
            if ($stmt->rowCount() > 0) {
                // Affichage des profils correspondants
                while ($row = $stmt->fetch()) {
                    echo "<div class='profile-container'>";
                    echo "<p>Pseudo: " . htmlspecialchars($row["Pseudo"]) . "</p>";
                    echo "<p>Prénom: " . htmlspecialchars($row["Prénom"]) . " - Nom: " . htmlspecialchars($row["nom"]) . "</p>";
                    // Afficher la photo si elle existe
                    if (!empty($row["Photo_de_profil"])) {
                        echo "<img src='" . htmlspecialchars($row["Photo_de_profil"]) . "' alt='Photo de profil' class='profile-image'>";
                    } else {
                        echo "<p>Pas de photo de profil disponible</p>";
                    }
                    echo "<form action='delete.php' method='post'>";
                    echo "<input type='hidden' name='id' value='" . $row["ID"] . "'>";
                    echo "<button type='submit' class='btn'>Supprimer</button>";
                    echo "</form>";
                    echo "</div>";
                }
            } else {
                echo "<p>Aucun profil trouvé</p>";
            }
        } catch (PDOException $e) {
            echo "<p>Erreur de connexion à la base de données: " . htmlspecialchars($e->getMessage()) . "</p>";
        }
        ?>
    </div>
</body>
</html>
