# <div align="center"> Projet de développement web :<br> CY Love </div>
<br>

## Description
### Généralités
&emsp; CY Love est un site web de rencontre qui permet aux utilisateurs de rechercher des types de relation spécifiques via des critères basés sur des fruits. Un système de filtres différencie les fonctionnalités accessibles aux abonnés de celles des non-abonnés. Dans la suite de ce document, la page *login.php* ci-dessous sera considéré comme la "page de référence" de notre site.

![Page de référence](Images_README/Connexion_page.png)

### Navigation
&emsp; La navigation sur le site se fait principalement depuis le menu déroulant (voir image ci-dessous) accessible sur toutes les pages du site en haut à droite. Certaines pages du menu possèdent des restrictions d'accès :
- La page « **Compte** » (renvoie à *personal-account.php*) : accessible uniquement aux utilisateurs connectés.
- La page « **Rechercher un compte** » (*search.php*) : n'est accessible que si l'utilisateur est connecté et abonné. S'il n'est pas connecté, il est redirigé vers la page de connexion (*login.php*). S'il est connecté mais pas abonné, il est redirigé vers la page d'abonnement *service.php*.
- La page « **Services** » (*service.php*) : accessible à tous (même aux visiteurs sans comptes). Cette page permet de voir les avantages que possèdent les utilisateurs abonnés au utilisateur non abonnés. Si la personne est connectée à son compte, elle peut choisir de souscrire à l'abonnement ou de le résilier.

![Menu déroulant](Images_README/Menu_déroulant.png)

<!--## Installation-->

## Création de compte
&emsp; Pour créer un compte, il suffit de se rendre sur la page *register.php*, accessible depuis *login.php* en cliquant sur le bouton « **S'inscrire** » en haut. Sur cette page vous devez rentrez votre prénom, nom, mail, pseudo et mot de passe. Après l'appui sur le bouton « Valider », un message vous indiquera que votre compte a bien été créé.
![Page d'inscription](Images_README/Inscription_page.png)

**Attention !** si l'un des champs n'est pas rempli ou si le pseudo entré est déjà utilisé par un autre utilisateur, un message d'erreur s'affichera sur la page.<br>
Exemples :
<div align="center">
    <img src="Images_README/ERREURmsg_champ_vide.png" alt="Erreur champ vide" width="50%">
    <img src="Images_README/ERREURmsg_pseudo_utilisé.png" alt="Pseudo déjà utilisé" width="43%">
</div>
<br>


## Utilisation

### Connexion
&emsp; Une fois l'étape de l'inscription effectuée, il suffit de se rendre sur la page *login.php* (accessible via le bouton « **Se connecter** », voir image 1) et d'entrer le pseudo et le mot de passedu compte. L'utilisateur sera ensuite automatiquement redirigé au bout de 3 secondes (grâce à la page *verif_login.php*) vers *personal-account.php* où il peut modifier ses informations personnelles.
<br><br>
<u>A noter :</u> En cas d'oubli du mot de passe, l'utilisateur peut cliquer sur le bouton « **Mot de passe oublié** ». Il sera redirigé vers *forgot.php* et pourra récupérer son mot de passe en entrant son pseudo et le mail associé à son compte (voir image ci-dessous).
![Page Mot de passe oublié](Images_README/MDP_oublié_page.png)

### Après connexion
&emsp; Après connexion, l'utilisateur arrive sur ses informations personnelles (*personal-account.php*) qu'il peut modifier ou compléter. Un menu à gauche permettra d'accéder :
- aux informations personnelles
- à la messagerie
- de se déconnecter

![Informations personnelles](Images_README/Informations_personnelles_page.png)


### Informations personnelles
&emsp; Dès qu'il est connecté, l'utilisateur peut compléter ou modifier les informations de son compte via la page *personal-account.php* accessible après connexion ou depuis le menu déroulant. Ses informations sont affichées et peuvent être modifiées par l'utilisateur. Après l'étape de connexion, l'utilisateur peut alors ajouter des informations supplémentaires le concernant comme :
- son **genre**
- sa **profession**
- une **photo de profil** (uniquement au format jpg) : si l'utilisateur en a choisie une, celle-ci est affichée en haut à droite à côté du menu déroulant.

### Abonnement
&emsp; L'utilisateur peut s'abonner en cliquant sur l'onglet « **Services** » dans le menu déroulant. Il arrive ensuite sur la page *service.php* et peut cliquer sur « **Je m'abonne** ». Si l'utilisateur est déjà abonné, il peut aussi cliquer sur le bouton « **se désabonner** ».
![Abonnement](Images_README/Abonnement_page.png)

<u>A noter :</u> L'abonnement est indispensable si l'utilisateur veut pouvoir échanger avec d'autres utilisateurs dans la messagerie.


### Recherche de profils
&emsp; Tous les utilisateur connectés peuvent voir les 10 premiers profils créés en cliquant sur « **Voir les profils** » (*10profil.php*) dans le menu déroulant.<br>
La recherche de profils est quant à elle accessible en cliquant sur « **Rechercher un compte** » (*search.php*) aux abonnés uniquement. Ils peuvent rechercher les utilisateurs dans la base de données en fonction de leur sexe ou de leur type de fruit. Une personne non abonée souhaitant se rendre sur la partie recherche sera renvoyé vers les services d'abonnements.
![Abonnement](Images_README/Recherche.PNG)


### Messagerie et chat
&emsp; La messagerie est accessible entre autres depuis la page d'informations personnelles en cliquant sur « **Messagerie** » dans le menu de gauche. L'utilisateur arrive donc sur la page *chat_history_contact.php* où il peut choisir avec quel destinataire il veut échanger. Pour cela il existe 2 moyens : 
- **échanger avec une personne avec qui il a déjà parlé** : ces destinataires apparraissent au centre de la page. Pour chaque destinataire des informations sont données comme le dernier message échangé, son heure d'envoi, l'expéditeur de ce message, la photo de profil du destinataire (si elle existe). Les destinataires apparaissent dans l'ordre anti-chronologique de dernier message envoyé. Pour échanger avec l'un de ces destinatires il suffit de cliquer sur le nom du destinataire.
- **échanger avec un nouveau destinataire** : pour cela il suffit de taper le pseudo de l'utilisateur avec qui il veut échanger dans la barre « **Nouveau destinataire** ». Si le destinataire recherché n'existe pas, l'utilisateur en sera informé.<br>
<div align="center">
    <img src="Images_README/ERREURmsg_faux_pseudo_Messagerie.png" alt="Faux_pseudo" width="65%">
</div>

<br>
Dans ces 2 cas (et si le destinataire existe), l'utilisateur est renvoyé vers la page *chat.php* où il pourra échanger avec le destinataire sélectionné.

<u>Image de la messagerie :</u>
![Messagerie](Images_README/Messagerie_page.png)


Une fois dans le chat, l'historique des messages échangés s'affiche par ordre chronologique avec la date et l'heure de chaque message (fuseau horaire : Europe/Paris). Pour envoyer un message, il suffit de le taper dans le champ prévu à cet effet et de l'envoyer.
<u>Image du chat :</u>
![Messagerie](Images_README/Chat_page.png)

**<u>Attention !</u>** La partie messagerie est accessible même aux non-abonnés mais l'échange des messages est <u>uniquement réservé aux abonnés</u>.<br>
**<u>A noter :</u>** La messagerie est **instantannée**, ce qui signifie que 2 utilisateurs peuvent échanger en même temps et voir les messages échangés sans rafraîchir la page car les messages sont récupérés toutes les 1s.<br>


## Stockage des données
&emsp; L'ensemble des données des utilisateurs sont stockées dans une base de données SQL appelée « cy_love_database ». Cette base de données possède 3 tableaux :
- user_info : Ce tableau stocke toutes les informations de compte des utilisateurs à savoir ID, pseudo, mot de passe, prénom, nom, mail, photo de profil, abonnement, fruit, genre, profession. C'est user_info qui est utilisé pour créer un nouveau compte, se connecter à un compte déjà existant ou modifier des informations de compte.
- messages : Ce tableau stocke les messages échangés par tous les utilisateurs. Il possède 5 colonnes et stocke l'ID du message, l'ID de l'expéditeur de message, l'ID du receveur, le message, la date d'envoi. C'est ce tableau qui est utilisé pour la messagerie et le chat (*chat_history_contact.php* et *chat.php*)
- bannis : Ce tableau stocke les mails des utilisateurs bannis par l'admin. Il possède 3 colonnes qui stockent : un ID, le mail du banni, la date où l'utilisateur a été banni. C'est ce tableau qui est utilisé à la connexion pour vérifier si l'utilisateur a été banni.
![Base de données](Images_README/Base_de_données.png)




## Administration
Bannissement : Les administrateurs peuvent bannir des utilisateurs ne respectant pas le règlement. Les utilisateurs bannis ne peuvent pas recréer de compte avec la même adresse mail. 

## Limites du Projet
Lors de la recherche de profils, l’utilisateur ne peut pas envoyer directement de message en cliquant sur le profil en question. Il doit taper le nom du profil dans la messagerie pour envoyer un message.<br>
Les mots de passe ne sont pas hasshés dans la base de données.
L'administrateur ne peut pas accéder à l'historique des conversations des abonées (Code implémenté et fonctionnel mais liaisons entre les codes non faites).

## Structure des Fichiers
header.php : En-tête commun pour toutes les pages.
login.php : Page de connexion et d'accueil.
verif_login.php : vérifie les informations pour se connecter.
logout.php : Déconnexion de compte.
forgot.php : Page de mot de passe oublié.
register.php : Page d'inscription.
verif_register.php : vérifie les informations pour créer un compte.
personal-account.php : Page des informations de l'utilisateur.
register_modification_account.php: enregistre les modifications d'informations personnelles du compte

search.php : Page de recherche de profils.
10profil.php : Page affichant les 10 prmiers profils créés.
service.php : Page d'abonnement.

account_icon_bar.php : menu de gauche une fois l'utilisateur connecté (affiché dans personal-account.php par ex).
chat_history_contact.php : Page d'historique des anciens destinataires (messagerie).
store_contact_id.php: utilisé dans chat_history_contact.php
chat.php : Page d'échange de messages.
Load_messages.php : Page chargée dans chat.php toutes les 1s pour afficher les messages échangés

verifi-abonee.php : vérifié si l'utilisateur est abonné.
admin.php : Page d'administration pour la gestion et suppression des utilisateurs.

Fichier CSS : Fichiers de style CSS pour le design des pages du site.

Dépendances
PHP : Pour la logique côté serveur.
SQL : Pour la gestion de la base de données des utilisateurs.
CSS : Pour le style et le design des pages web.
Jquery : Pour l'instantanéité dans l'échange des messages (chat.php)

## Répartition des tâches
- Thomas RYKACZEWSKI : Logo CY Love, base de données SQL, chat, sessions, pages de redirection, menus déroulant et fixe, readme.
- Adam BEN HAMIDA : Style du site, barre de recherche, Mot de passe oublié, Admin, liens, Affichage 10 profils.
- Thomas PISANESCHI : Filtre abonnés et non abonnés.
- Heeshaam FOWDAR : ReadMe, recherches internet et explications sur le fonctionnement de mySQL (commandes, ...), de base de données, clés primaires/étrangères.

## Auteurs
- Thomas RYKACZEWSKI
- Adam BEN HAMIDA
- Thomas PISANESCHI
- Heeshaam FOWDAR


## Informations
Professeur d'informatique : Souhila ARIB<br>
Classe : PréIng2 MI2 (CY Tech, Cergy)<br>
Date finale de rendu : vendredi 7 juin 2024<br>
Lien Gitlab : https://gitlab.com/cy-trucks-pr-ing-2/cy-love