<?php
// Démarrer la session seulement si elle n'est pas déjà active
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Utiliser le tampon de sortie pour éviter les problèmes de redirection
ob_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style3.css">
    <title>CY LOVE</title>
</head>
<body>
    <div class="wrapper">
        <?php include 'header.php'?>
        <div class="form-box">
            <div class="login-container" id="modify-account">
                <form action="update_account.php" method="POST" enctype="multipart/form-data">
                    <div class="top">
                        <header>Modifier les informations du compte</header>
                        <?php
                            if(isset($_SESSION['error_msg'])){
                                echo "<div style=\"color: rgb(255, 50, 50)\">" . htmlspecialchars($_SESSION['error_msg']) . "</div>";
                                unset($_SESSION['error_msg']); // remove session variable
                            }
                        ?>
                    </div>
                    <div class="input-box">
                        <input type="text" name="Pseudo" class="input-field" placeholder="Nouveau Pseudo" required>
                        <i class="bx bx-user"></i>
                    </div>
                    <div class="input-box">
                        <input type="password" name="Password" class="input-field" placeholder="Nouveau Mot de Passe" required>
                        <i class="bx bx-lock-alt"></i>
                    </div>
                    <div class="input-box">
                        <input type="text" name="Firstname" class="input-field" placeholder="Prénom" required>
                        <i class="bx bx-user"></i>
                    </div>
                    <div class="input-box">
                        <input type="text" name="Name" class="input-field" placeholder="Nom" required>
                        <i class="bx bx-user"></i>
                    </div>
                    <div class="input-box">
                        <input type="email" name="Email" class="input-field" placeholder="Email" required>
                        <i class="bx bx-envelope"></i>
                    </div>
                    <div class="input-box">
                        <input type="file" name="Profile_picture" class="input-field">
                        <i class="bx bx-image"></i>
                    </div>
                    <div class="input-box">
                        <input type="text" name="Profession" class="input-field" placeholder="Profession">
                        <i class="bx bx-briefcase"></i>
                    </div>
                    <div class="input-box">
                        <input type="submit" name="submit" class="submit" value="Mettre à jour">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $servername = "localhost";
        $login = "root";
        $pass = "";

        try {
            $connexion = new PDO("mysql:host=$servername;dbname=cy_love_database", $login, $pass);
            $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }

            /* -------------- Variables -------------- */
            $new_Pseudo = $_POST["Pseudo"];
            $new_Password = $_POST["Password"];
            $new_Firstname = $_POST["Firstname"];
            $new_Name = $_POST["Name"];
            $new_Email = $_POST["Email"];
            $new_Profession = $_POST["Profession"];
            $ID = $_SESSION["ID"];

            // Variables pour la photo de profil
            $file_user = "Accounts/ID_" . $ID;
            $target_file = $file_user . "/profile_picture/profile_picture_ID_" . $ID . ".jpg";
            $uploadOK = 1;
            $imgFileType = strtolower(pathinfo(basename($_FILES['Profile_picture']['name']), PATHINFO_EXTENSION));

            /* -------------- Vérifications -------------- */
            if (strlen($new_Password) <= 0 || strlen($new_Pseudo) <= 0 || strlen($new_Email) <= 0 || strlen($new_Firstname) <= 0 || strlen($new_Name) <= 0 || strlen($new_Profession) <= 0) {
                $_SESSION['error_msg'] = "Aucun champ ne doit être vide !";
                header("Location: personal-account.php");
                exit;
            }
            if ($imgFileType !== 'jpg' && $imgFileType !== '') {
                $_SESSION['error_msg'] = "Seul le format JPG est accepté pour les photos de profil.";
                header("Location: personal-account.php");
                exit;
            }

            /* -------------- Récupérer les informations du compte -------------- */
            $query_info_account = $connexion->prepare("SELECT * FROM user_info WHERE ID = :id");
            $query_info_account->bindParam(':id', $ID, PDO::PARAM_INT);
            $query_info_account->execute();
            $array_info_account = $query_info_account->fetchAll(PDO::FETCH_NUM);

            /* -------------- Modifier le pseudo -------------- */
            $query1 = $connexion->prepare("SELECT ID, Pseudo FROM user_info WHERE Pseudo = :pseudo");
            $query1->bindParam(':pseudo', $new_Pseudo, PDO::PARAM_STR);
            $query1->execute();
            $array_same_pseudo = $query1->fetchAll(PDO::FETCH_NUM);

            if (count($array_same_pseudo) >= 2) {
                $_SESSION['error_msg'] = "ERREUR ETRANGE n°1000.<br>Le pseudo " . $new_Pseudo . " est déjà pris par AU MOINS un autre utilisateur !<br>Veuillez en choisir un autre.";
                header("Location: personal-account.php");
                exit;
            } elseif (count($array_same_pseudo) == 1) {
                if ($array_same_pseudo[0][0] == $ID) {
                    // Pseudo non modifié
                } else {
                    $_SESSION['error_msg'] = "Le pseudo " . $new_Pseudo . " est déjà pris par un autre utilisateur !<br>Veuillez en choisir un autre";
                    header("Location: personal-account.php");
                    exit;
                }
            } else { // Nouveau pseudo correct
                $query_pseudo_update = $connexion->prepare("UPDATE user_info SET Pseudo = :pseudo WHERE ID = :id");
                $query_pseudo_update->bindParam(':pseudo', $new_Pseudo, PDO::PARAM_STR);
                $query_pseudo_update->bindParam(':id', $ID, PDO::PARAM_INT);
                $query_pseudo_update->execute();
                $_SESSION['Pseudo'] = $new_Pseudo;
            }

            /* -------------- Modifier les informations du compte -------------- */
            $query2 = $connexion->prepare("UPDATE user_info SET Mot_de_passe = :password, Prénom = :firstname, Nom = :name, Email = :email, Profession = :profession WHERE ID = :id");
            $query2->bindParam(':password', $new_Password, PDO::PARAM_STR);
            $query2->bindParam(':firstname', $new_Firstname, PDO::PARAM_STR);
            $query2->bindParam(':name', $new_Name, PDO::PARAM_STR);
            $query2->bindParam(':email', $new_Email, PDO::PARAM_STR);
            $query2->bindParam(':profession', $new_Profession, PDO::PARAM_STR);
            $query2->bindParam(':id', $ID, PDO::PARAM_INT);
            $query2->execute();

            /* -------------- Modifier ou ajouter une photo de profil -------------- */
            if (!file_exists("Accounts/")) {
                mkdir("Accounts/");
            }
            if (!file_exists($file_user)) {
                mkdir($file_user);
                mkdir($file_user . "/profile_picture");
            }

            if (!empty($_FILES["Profile_picture"]["tmp_name"]) && isset($_POST["submit"])) {
                $check = getimagesize($_FILES["Profile_picture"]["tmp_name"]);
                if ($check !== false) {
                    $uploadOK = 1;
                } else {
                    $_SESSION['error_msg'] = "Le fichier n'est pas une image.";
                    $uploadOK = 0;
                    header("Location: personal-account.php");
                    exit;
                }
            } else {
                $uploadOK = -1;
            }

            if ($uploadOK == 1) {
                if (move_uploaded_file($_FILES["Profile_picture"]["tmp_name"], $target_file)) {
                    $query_profile_picture = $connexion->prepare("UPDATE user_info SET Photo_de_profil = :photo WHERE ID = :id");
                    $query_profile_picture->bindParam(':photo', $target_file, PDO::PARAM_STR);
                    $query_profile_picture->bindParam(':id', $ID, PDO::PARAM_INT);
                    $query_profile_picture->execute();
                } else {
                    $_SESSION['error_msg'] = "Désolé, une erreur est survenue lors du téléchargement du fichier.";
                    header("Location: personal-account.php");
                    exit;
                }
            } elseif ($uploadOK == 0) {
                $_SESSION['error_msg'] = "Votre fichier n'a pas été téléchargé.";
                header("Location: personal-account.php");
                exit;
            }
            $_SESSION['success_msg'] = "Vos informations ont été mises à jour.";
            header("Location: personal-account.php");
            exit;
        } catch (PDOException $e) {
            echo "Connexion impossible : " . htmlspecialchars($e->getMessage());
        }
    }
    ob_end_flush();
    ?>
</body>
</html>
