-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 07 juin 2024 à 07:35
-- Version du serveur : 8.2.0
-- Version de PHP : 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cy_love_database`
--

-- --------------------------------------------------------

--
-- Structure de la table `bannis`
--

DROP TABLE IF EXISTS `bannis`;
CREATE TABLE IF NOT EXISTS `bannis` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `date_ban` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `bannis`
--

INSERT INTO `bannis` (`id`, `email`, `date_ban`) VALUES
(1, 'juli_2.0@hotmailwiskicaca.com', '2024-06-06'),
(2, 'MAIL1', '2024-06-06'),
(3, 'MAIL2', '2024-06-06'),
(4, 'MAIL4', '2024-06-06'),
(5, 'MAIL5', '2024-06-06'),
(6, 'MAIL6', '2024-06-06'),
(7, 'david.guetta@gmail.com', '2024-06-06'),
(8, 'taylor.swift@eras-tour.com', '2024-06-06'),
(9, 'shakira@gmail.com', '2024-06-06'),
(10, 'admin@gmail.com', '2024-06-06');

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `ID_msg` int NOT NULL AUTO_INCREMENT,
  `ID_user_sending` int NOT NULL,
  `ID_user_receiving` int NOT NULL,
  `Message` text,
  `Date` text NOT NULL,
  PRIMARY KEY (`ID_msg`),
  KEY `FK_PersonOrder` (`ID_user_sending`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `messages`
--

INSERT INTO `messages` (`ID_msg`, `ID_user_sending`, `ID_user_receiving`, `Message`, `Date`) VALUES
(8, 10, 2, 'Salut PS2,<br />\r\nc\'est Shakira !', ''),
(7, 1, 3, 'Hey PS3, c\'est PS1.<br />\r\nca va?', ''),
(6, 1, 2, 'C bon je crois', ''),
(68, 10, 1, 'Salut PS1, c\'est Shakira!', ''),
(61, 2, 10, 'Salut Shakira, c\'est PS2 !', ''),
(62, 10, 2, 'On va compter, je commence :<br />\r\n100 !', ''),
(57, 2, 10, 'Salut Shakira, c\'est PS2 !', ''),
(58, 10, 2, 'je fais trop de tests.<br />\r\n', ''),
(67, 10, 1, 'Salut PS1, c\'est Shakira!', ''),
(66, 2, 10, 'OK , je continue : 101 !', ''),
(69, 12, 14, 'Salut', ''),
(70, 12, 18, 'yo', ''),
(71, 14, 12, 'Hola', ''),
(72, 14, 18, 'Ciao<br />\r\n', ''),
(73, 14, 12, 'ca va<br />\r\n', ''),
(74, 18, 17, 'Salut je t\'aime', ''),
(75, 18, 17, 'Ca va ?', ''),
(76, 17, 18, 'Oui et toi<br />\r\n', ''),
(77, 10, 1, 'test', ''),
(78, 10, 1, 'ca va?', ''),
(79, 1, 10, 'Oui et toi ?', ''),
(80, 10, 1, 'C\'est correct', ''),
(81, 1, 10, 'Vas y explique', ''),
(82, 1, 10, 'Alors ?', ''),
(83, 1, 3, 'je veux bien une réponse stp', ''),
(84, 10, 1, 'nan trop long à écrire', ''),
(91, 10, 1, 'je te dirai plus tard', ''),
(97, 10, 8, 'Salut mon David!<br />\r\nT\'as un nouveau hit dans les tuyaux ?', ''),
(99, 10, 8, 'perso j\'ai peut-être une idée mais je voudrais ton avis.<br />\r\nMuchos besos !', '2024-06-07 07:42:03'),
(100, 8, 10, 'OK ma Shaki, on peut se voir jeudi pour en parler.<br />\r\nTu seras bien à Ibiza ??', '2024-06-07 08:44:25');

-- --------------------------------------------------------

--
-- Structure de la table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
CREATE TABLE IF NOT EXISTS `user_info` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Pseudo` varchar(100) DEFAULT NULL,
  `Mot_de_passe` varchar(50) DEFAULT NULL,
  `Prénom` varchar(100) DEFAULT NULL,
  `Nom` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Photo_de_profil` varchar(255) NOT NULL,
  `Abonnement` text,
  `Preference` varchar(255) DEFAULT NULL,
  `Genre` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Profession` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `user_info`
--

INSERT INTO `user_info` (`ID`, `Pseudo`, `Mot_de_passe`, `Prénom`, `Nom`, `Email`, `Photo_de_profil`, `Abonnement`, `Preference`, `Genre`, `Profession`) VALUES
(18, 'Riri', '12', 'Rihanna', 'Diamond', 'riri@gmail.com', 'Accounts/ID_18/profile_picture/profile_picture_ID_18.jpg', 'OUI', 'Raisin', 'Madame', 'Chanteuse'),
(17, 'BlueEyes', '1234', 'Gojo', 'Satoru', 'gojo@gmail.com', 'Accounts/ID_17/profile_picture/profile_picture_ID_17.jpg', 'OUI', 'Peche', 'Monsieur', 'Magicien Professionel'),
(11, 'Cynthia', '1234', 'Cynthia', 'Sinnoh', 'cynthia@gmail.com', 'Accounts/ID_11/profile_picture/profile_picture_ID_11.jpg', 'OUI', 'Cerise', '', 'Maitre de la ligue'),
(12, 'Spidey', 'salut', 'Peter', 'Parker', 'Peter@gmail.com', 'Accounts/ID_12/profile_picture/profile_picture_ID_12.jpg', 'OUI', 'Pasteque', 'Monsieur', 'Acteur/ Super Héros'),
(13, 'MJ', '1234', 'Zendaya', 'Clark', 'zendaya@gmail.com', 'Accounts/ID_13/profile_picture/profile_picture_ID_13.jpg', NULL, 'Raisin', 'Madame', 'Actrice'),
(14, 'Chili', '1234', 'Carlos', 'Sainz', 'carlos@gmail.com', 'Accounts/ID_14/profile_picture/profile_picture_ID_14.jpg', 'OUI', 'Peche', 'Monsieur', 'Pilote de F1 pour Ferrari'),
(15, 'Charlie', '1234', 'Charles', 'Leclerc', 'Leclerc@gmail.fr', 'Accounts/ID_15/profile_picture/profile_picture_ID_15.jpg', NULL, 'Cerise', 'Monsieur', 'Pilote'),
(16, 'Barbie', '1234', 'Margot', 'Robie', 'margot@gmail.com', 'Accounts/ID_16/profile_picture/profile_picture_ID_16.jpg', NULL, 'Raisin', 'Madame', 'Actrice/Mannequin'),
(1, 'PS1', 'MDP1', 'PRENOM1', 'NOM1', 'MAIL1', '', 'OUI', NULL, '', NULL),
(2, 'PS2', 'MDP2', 'PRENOM2', 'NOM2', 'MAIL2', '', NULL, NULL, '', NULL),
(3, 'PS3', 'MDP3', 'PRENOM3', 'NOM3', 'MAIL3', '', NULL, NULL, '', NULL),
(7, 'flo_dessin', 'chiasse2.0', 'Flora', 'Ryka', 'juli_2.0@hotmailwiskicaca.com', '', 'OUI', NULL, '', NULL),
(8, 'david_guetta', 'MDPdg', 'David', 'Guetta', 'david.guetta@gmail.com', 'Accounts/ID_8/profile_picture/profile_picture_ID_8.jpg', 'OUI', NULL, '', NULL),
(9, 'TS', 'MDPts', 'Taylor', 'Swift', 'taylor.swift@eras-tour.com', 'Accounts/ID_9/profile_picture/profile_picture_ID_9.jpg', 'OUI', NULL, '', NULL),
(10, 'Shakira', 'MDPshakira', 'Shakira', 'Isabel Mebarak Ripoll', 'shakira@gmail.com', 'Accounts/ID_10/profile_picture/profile_picture_ID_10.jpg', 'OUI', NULL, '', 'Chanteuse');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
