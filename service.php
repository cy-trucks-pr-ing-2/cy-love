<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="service_style.css">
    <title>CY LOVE</title>
</head>
<body style="background-image: url('Images/Background_images.jpg')">
    <?php include 'header.php'; ?>
    <?php include 'account_icon_bar.php'; ?>
    <nav class="nav">
        <div class="nav-logo">
            <img src="Logos/Logo_CY_Love.svg" alt="Logo CY Love" height="70px" onclick="document.location='login.php'" style="cursor: pointer;">
        </div>
        <div class="menu-btn" id="menu-btn" onclick="myMenuFunction()">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div>
    </nav>
    <script>
        function myMenuFunction() {
            var i = document.getElementById("menu-btn");
            var j = document.getElementById("navMenu");
            if(i.className === "menu-btn") {
                i.className += " responsive";
                j.className += " responsive";
            } else {
                i.className = "menu-btn";
                j.className = "nav-menu";
            }
        }
    </script>

    <div class="profile-container">
        <img src="path_to_profile_image.jpg" alt="Profile Image" class="profile-image">
        <p>Nom: John Doe</p>
        <p>Email: john.doe@example.com</p>
    </div>

    <div class="free-block owned" id="free-block">
        <div class="top-free-block">
            <p>GRATUIT</p>
        </div>
        <div class="text-free-block">
            <p>&#x274C; Accès au Chat</p>
            <p>&#x274C; Recherche de profils</p>
            <p>&#x274C; Bonus Mise en Avant</p>
            <p>&#x274C; Zéro Pub</p>
        </div>
        <form action="update_subscription.php" method="post">
            <input type="hidden" name="action" value="unsubscribe">
            <button type="submit" id="free-block-btn" class="free-block-btn">&#128128; Se désabonner &#128128;</button>
        </form>
    </div>

    <div class="sub-block" id="sub-block">
        <div class="top-sub-block">
            <p>PREMIUM</p>
        </div>
        <div class="text-sub-block">
            <p>✅ Accès au Chat</p>
            <p>✅ Recherche de profils</p>
            <p>✅ Bonus Mise en Avant</p>
            <p>✅ Zéro Pub</p>
        </div>
        <form action="update_subscription.php" method="post">
            <input type="hidden" name="action" value="subscribe">
            <button type="submit" class="sub-block-btn" id="sub-block-btn">&#128571; Je m'abonne ! &#128571;</button>
        </form>
    </div>

    <?php
    if (isset($message)) {
        echo "<div style='color: red; text-align: center; margin-top: 20px;'>$message</div>";
    }
    ?>
</body>
</html>
