<?php
    session_start();
    if ( isset($_SESSION['is_connected']) && $_SESSION['is_connected'] == 'oui' && isset($_SESSION['ID']) && isset($_SESSION['Pseudo']) ){
        header("Location: personal-account.php");
        exit;
    }
    elseif( !isset($_POST["submit"]) ){
        //someone not connected on this page ->redirection
        header('Location: login.php');
        exit;
    }
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <script>
        var n = 4; // en secondes (n-1 secondes exécutées)
        var x = setInterval(countdown, 1000); //call countdown (without '()') function every 1000 milliseconds

        // Fonction compte à rebours (countdown)
        function countdown(){
            n--;
            document.querySelector("#countdown_redirection span").innerHTML = n;
            if(n <= 0){
                clearInterval(x);
                window.location.href = "personal-account.php";
            }
        }
    </script>
    <link rel="stylesheet" href="style2.css">
    <title>CY LOVE</title>
</head>

<body style="background-image: url('Images/Background_images.jpg')"> <!--Background image : https://img.freepik.com/photos-gratuite/jeune-couple-romantique-sexy-amoureux-heureux-plage-ete-ensemble-s-amusant-portant-maillots-bain-montrant-signe-du-coeur-sundet_285396-6545.jpg?t=st=1715103572~exp=1715107172~hmac=144c7e5b0ff875c6caeab703b9f2860b0da711ca04f6eb9e9186eb8b7e9f819d&w=2000-->
    <div class="wrapper">
        <?php include 'header.php'?>
        <div id="countdown_redirection" style="text-align: left; color: white; display: flex; justify-content: center; align-items: center">
            <?php
                // Add account in cylove
                $servername = "localhost";
                $login = "root";
                $pass = "";

                // Server connection test
                try{
                    $connexion = new PDO("mysql:host=$servername;dbname=cy_love_database", $login, $pass);
                    $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //PDO error mode
                    //echo "Connexion à la base de données réussie";

                    $Pseudo = $_POST["Pseudo"];
                    $Password =  $_POST["Password"];
                    if( strlen($Password)<=0 || strlen($Pseudo)<=0 ){
                        $_SESSION['error_msg'] = "Aucun champ ne doit être vide !";
                        header("Location: login.php");
                        exit;
                    }

                    if ($Pseudo == 'Admin' && $Password == 'admin') {
                        header("Location: admin.php");
                        exit;
                    }

                    // TEST if the pseudo exists in database
                    $query_all_pseudos = $connexion->prepare("SELECT Pseudo FROM user_info");//query = requête
                    $query_all_pseudos->execute();
                    $array_all_pseudos = $query_all_pseudos->fetchall(); // array with all pseudos in database
                    for ($i=0; $i<count($array_all_pseudos); $i++){
                        if ($Pseudo == $array_all_pseudos[$i][0]){
                            $query_pwd = $connexion->prepare(
                                "SELECT Mot_de_passe
                                FROM user_info
                                WHERE Pseudo = :pseudo"
                            );
                            $query_pwd->bindParam(':pseudo', $Pseudo);
                            $query_pwd->execute();
                            $result_pwd = $query_pwd->fetchall(); // array with the correct password

                            $_SESSION['Pseudo'] = $Pseudo;
                            if($Password == $result_pwd[0][0]){
                                echo "<p>";
                                echo "<br>Bienvenue " . $Pseudo . "<br>Mot de passe : " . $Password;

                                // Get user ID
                                $query_get_ID = $connexion->prepare(
                                    "SELECT ID
                                    FROM user_info
                                    WHERE Pseudo = :pseudo"
                                );
                                $query_get_ID->bindParam(':pseudo', $Pseudo);
                                $query_get_ID->execute();
                                $array_user_ID = $query_get_ID->fetchall(PDO::FETCH_NUM); // array with user ID
                                $_SESSION['ID'] = $array_user_ID[0][0];
                                //Variables to stay connected
                                $ID = $_SESSION['ID'];
                                $_SESSION['is_connected'] = 'oui';

                                    echo "<br>Connexion réussie.";
                                    echo "<br>Redirection dans <span></span> seconde(s).";
                                    echo "</p>";
                                exit;
                            }
                            else{
                                header("Location: login.php");
                                exit;
                            }
                        }
                    }
                    $_SESSION['error_msg'] = "Pseudo inexistant !";
                    header("Location: login.php");
                    exit;
                }

                catch (PDOException $e){
                    echo "Connexion impossible : " . $e->getMessage();
                }
            ?>
        </div>
        <h1>Recherche de profils</h1>
        <form action="search.php" method="GET" class="form-container">
            <div class="input-box">
                <input type="text" id="keyword" name="keyword" class="input-field" placeholder="Mot-clé">
                <button type="submit" class="submit">
                    <span>Rechercher</span>
                    <i class="bx bx-search"></i>
                </button>
            </div>
        </form>
    </div>
</body>
</html>
