<?php
    session_start();
    
    $ID = $_SESSION['ID'];
    $file_user = "ID_" . $ID;

    if(file_exists($file_user)){
        //echo $file_user . " existe.";
    }
    else{ // create directory
        mkdir($file_user);
        //echo "<br>fichier " . $file_user . " créé.";
        mkdir($file_user . "/profile_picture");
        //echo "<br>fichier " . $file_user . "/profile_picture créé.";
    }

    //upload profile_picture
    $target_file = $file_user . "/profile_picture/" . basename($_FILES['Profile_picture']['name']); //basename() : show filename
    $uploadOK = 1;
    $imgFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION)); //get image extension

    //check if an img was selected to be uploaded + if the form is submitted
    if( !empty($_FILES["Profile_picture"]["tmp_name"]) && isset($_POST["submit"])) {
        $check = getimagesize($_FILES["Profile_picture"]["tmp_name"]); // getimagesize(): get image infos, or return false if this is not an img
        echo "<br>";
        print_r($check);
        if($check !== false) {
            echo "<br>File is an image - " . $check["mime"] . ".";
            $uploadOK = 1;
        }
        else {
            echo "<br>File is not an image.";
            $uploadOK = 0;
        }
    }

    if ($uploadOK == 1) {
        if (move_uploaded_file($_FILES["Profile_picture"]["tmp_name"], $target_file)) {
            echo "The file ". htmlspecialchars( basename( $_FILES["Profile_picture"]["name"])). " has been uploaded.";
        }
        else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
    else {
        echo "Sorry, your file was not uploaded."; 
    }

?>