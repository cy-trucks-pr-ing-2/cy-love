<?php
session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $servername = "localhost";
    $login = "root";
    $pass = "";
    $dbname = "cy_love_database";

    try {
        $connexion = new PDO("mysql:host=$servername;dbname=$dbname", $login, $pass);
        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        /* -------------- Variables -------------- */
        $gender = $_POST["gender"];
        $firstname = $_POST["Firstname"];
        $lastname = $_POST["Name"];
        $email = $_POST["Email"];
        $pseudo = $_POST["Pseudo"];
        $password = $_POST["Password"];
        $preference = $_POST["Preference"];

        // Check if the email is in the banned_emails table
        $query_banned = $connexion->prepare("SELECT Email FROM bannis WHERE Email = :email");
        $query_banned->bindParam(':email', $email, PDO::PARAM_STR);
        $query_banned->execute();
        $banned_email = $query_banned->fetch(PDO::FETCH_ASSOC);

        if ($banned_email) {
            $_SESSION['error_msg'] = "Votre adresse email a été bannie. Veuillez contacter le support.";
            header("Location: register.php");
            exit;
        }

        // Vérification du pseudo unique
        $query1 = $connexion->prepare("SELECT ID FROM user_info WHERE Pseudo = :pseudo");
        $query1->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
        $query1->execute();
        $array_same_pseudo = $query1->fetchAll(PDO::FETCH_NUM);

        if (count($array_same_pseudo) > 0) {
            $_SESSION['Pseudo'] = $pseudo;
            $_SESSION['error_msg'] = "Le pseudo est déjà utilisé, veuillez en choisir un autre.";
            header("Location: register.php");
            exit;
        } else {
            // Insertion des données utilisateur dans la base de données
            $query2 = $connexion->prepare("INSERT INTO user_info (Prénom, Nom, Email, Pseudo, Mot_de_passe, Preference, Genre) VALUES (:firstname, :lastname, :email, :pseudo, :password, :preference, :gender)");
            $query2->bindParam(':firstname', $firstname, PDO::PARAM_STR);
            $query2->bindParam(':lastname', $lastname, PDO::PARAM_STR);
            $query2->bindParam(':email', $email, PDO::PARAM_STR);
            $query2->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
            $query2->bindParam(':password', $password, PDO::PARAM_STR);
            $query2->bindParam(':preference', $preference, PDO::PARAM_STR);
            $query2->bindParam(':gender', $gender, PDO::PARAM_STR);
            $query2->execute();

            // Redirection vers la page de connexion après inscription réussie
            header("Location: login.php");
            exit;
        }
    } catch (PDOException $e) {
        echo "Erreur de connexion : " . htmlspecialchars($e->getMessage());
    }
} else {
    header("Location: register.php");
    exit;
}
?>
