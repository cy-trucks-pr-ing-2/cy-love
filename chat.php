<?php
session_start();
include 'verifi-abonee.php';
$ID = $_SESSION['ID'];
$Pseudo = htmlspecialchars($_SESSION['Pseudo']);
date_default_timezone_set('Europe/Paris'); // Configure le fuseau horaire
?>

<?php
$servername = "localhost";
$login = "root";
$pass = "";

// Connexion à la base de données
try {
    $connexion = new PDO("mysql:host=$servername;dbname=cy_love_database", $login, $pass);
    $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Envoyer un message
    if(isset($_POST['submit'])){
        if(isset($_POST['Pseudo_receiver'])){ // Cas pour une 1ere écriture à un nouveau contact
            $_SESSION['Pseudo_receiver'] = $_POST['Pseudo_receiver'];
        }
        if(!empty($_SESSION['Pseudo_receiver']) ){ //&& !empty($_POST['message'])
            $Pseudo_receiver = $_SESSION['Pseudo_receiver'];
            $query_receiver = $connexion->prepare("SELECT Pseudo, ID FROM user_info WHERE BINARY Pseudo = :pseudo"); //BINARY rend la comparaison sensible à la casse
            $query_receiver->bindParam(':pseudo', $Pseudo_receiver, PDO::PARAM_STR);
            $query_receiver->execute();
            $Infos_receiver = $query_receiver->fetchAll(PDO::FETCH_NUM);
            if(count($Infos_receiver) == 0){
                $_SESSION["error_msg"] = "Le pseudo " . $Pseudo_receiver . " n'existe pas.";
                header("Location: chat_history_contact.php");
                exit;
            } elseif(count($Infos_receiver) >= 2){
                $_SESSION["error_msg"] = "ERREUR anormale : le pseudo " . $Pseudo_receiver . " existe au moins 2 fois dans la base de données. Aucun message n'a été envoyé.";
                header("Location: chat_history_contact.php");
                exit;
            }


            // Le pseudo existe: ENVOYER MESSAGE (s'il existe)
            else {
                $ID_receiver = $Infos_receiver[0][1];
                $_SESSION['ID_receiver'] = $ID_receiver;
                if( isset($_POST['message']) && !empty($_POST['message']) ){
                    $message = nl2br($_POST['message']);
                    $current_datetime = date('Y-m-d H:i:s'); //get actual date and time
                    $query_message = $connexion->prepare("INSERT INTO messages (ID_user_sending, ID_user_receiving, Message, Date) VALUES (:id_user_sending, :id_user_receiving, :message, :current_datetime)");
                    $query_message->bindParam(':id_user_sending', $ID);
                    $query_message->bindParam(':id_user_receiving', $ID_receiver);
                    $query_message->bindParam(':message', $message);
                    $query_message->bindParam(':current_datetime', $current_datetime);
                    $query_message->execute();
                    unset($_POST['message']);
                    unset($_POST['submit']);
                    unset($message);
                }
                elseif( !isset($_POST['message']) ){
                    //do nothing
                    //La personne vient de se connecter à un nouveau contact, lorqu'elle arrive sur la page, elle n'a pas pu écrire de message
                }
                else{
                    $_SESSION["error_msg"] = "ERREUR : Vous ne pouvez pas envoyer de message vide.";
                    header("Location: chat.php");
                    exit;
                }
            }
        }
        else {
            $_SESSION["error_msg"] = "ERREUR : Le pseudo est vide";
            header("Location: chat_history_contact.php");
            exit;
        }
    }
} catch (PDOException $e) {
    echo "Connexion impossible à la base de données: " . htmlspecialchars($e->getMessage());
    exit;
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style2.css">
    <style>
        .chat-area{
            display: block;
            margin-top: 70px;
            width: 60%;
        }
        #messages{
            display: block;
            width: 100%;
            height : 400px;
            overflow: auto;
        }
        #messages .msg_receiving{
            display: block;
            max-width: 60%;
            margin-right: auto;
            margin-left: 0;
            margin-bottom: 10px;

            color: black;
            text-align: left;
            border-radius: 0px 15px 15px 15px;
            padding: 5px;
            background-color: rgb(255,192,203);
        }
        #messages .msg_sending{
            display: block;
            max-width: 60%;
            margin-right: 0;
            margin-left: auto;
            margin-bottom: 10px;
            color: black;
            text-align: right;
            border-radius: 15px 0px 15px 15px;
            background-color: rgb(192,255,203);
            padding: 5px;
        }

        .write-message-container{
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;
            margin-right: auto;
            margin-left: auto;
            text-align: center;
            width: 100%;
        }

        .input-field-message {
            font-size: 15px;
            background: rgba(255, 255, 255, 0.2);
            color: #fff;
            height: 50px;
            width: 100%;
            padding: 0 20px;
            padding-left: 45px;
            border: none;
            border-radius: 30px;
            outline: none;
            transition: .2s ease;
        }

        .input-field-message:hover, .input-field-message:focus {
            background: rgba(255, 255, 255, 0.25);
        }

        .submit-message {
            font-size: 15px;
            font-weight: 500;
            color: black;
            height: 45px;
            width: 30%;
            border: none;
            border-radius: 30px;
            outline: none;
            background: rgba(255, 255, 255, 0.7);
            cursor: pointer;
            transition: .3s ease-in-out;
        }

        .submit-message:hover {
            background: rgba(255, 255, 255, 0.5);
            box-shadow: 1px 5px 7px 1px rgba(0, 0, 0, 0.2);
        }
    </style>

    <title>CY LOVE</title>
</head>
<body style="background-image: url('Images/Background_images.jpg')">
    <div class="wrapper">
        <?php include 'header.php'; ?>
        <?php include 'account_icon_bar.php'?>
        <div class="chat-area">
            <section id="messages" style="display: block;"></section>
            <div class="write-message-container">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="top">
                        <?php
                        if(isset($_SESSION['error_msg'])){
                            echo "<div style=\"color: rgb(255, 50, 50)\">" . htmlspecialchars($_SESSION['error_msg']) . "</div>";
                        }
                        unset($_SESSION['error_msg']);
                        ?>
                    </div>
                    <div class="input-box">
                        <label for="message">Votre message</label>
                        <textarea name="message" id="message" class="input-field-message" placeholder="Tapez votre message ici" rows="3" required></textarea>
                        <i class="bx bx-envelope"></i>
                    </div>
                    <div class="input-box">
                        <input type="submit" name="submit" class="submit-message" value="Envoyer">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script>
        var x = setInterval(loadMessages, 1000);
        function loadMessages() {
            $('#messages').load('Load_messages.php');
        }
    </script>
</body>
</html>
