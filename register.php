<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style4.css">
    <title>CY LOVE</title>
</head>

<body style="background-image: url('Images/Background_images.jpg')">
    <div class="wrapper">
        <?php include 'header.php'?>
        <div class="register-container" id="register">
            <form action="verif_register.php" method="POST">
                <div class="top">
                    <header>S'inscrire</header>
                    <span>J'ai déjà un compte ? <a href="login.php" onclick="login()">Se connecter</a></span>

                    <?php
                        if (isset($_SESSION['Pseudo'])) {
                            echo "<div style=\"color: rgb(255, 50, 50)\">
                                <b style=\"color: rgb(255, 0, 0)\">ATTENTION !</b>
                                Le pseudo " . htmlspecialchars($_SESSION['Pseudo']) . " est déjà utilisé par un autre utilisateur, veuillez en saisir un autre.
                                </div>";
                        }
                        if (isset($_SESSION['error_msg'])) {
                            echo "<div style=\"color: rgb(255, 50, 50)\">" . htmlspecialchars($_SESSION['error_msg']) . "</div>";
                        }
                        session_unset(); // remove session variables => remove $_SESSION['Pseudo']
                    ?>
                </div>
                <div class="input-box">
                    <label for="gender">Genre</label>
                    <input type="radio" name="gender" value="Madame" required>Madame
                    <input type="radio" name="gender" value="Monsieur" required>Monsieur
                    <input type="radio" name="gender" value="Non binaire" required>Non binaire
                    <input type="radio" name="gender" value="Non défini" required>Non défini
                </div>
                <div class="input-box">
                    <input type="text" name="Firstname" class="input-field" placeholder="Prénom" required>
                    <i class="bx bx-user"></i>
                </div>
                <div class="input-box">
                    <input type="text" name="Name" class="input-field" placeholder="Nom" required>
                    <i class="bx bx-user"></i>
                </div>
                <div class="input-box">
                    <input type="email" name="Email" class="input-field" placeholder="Email" required>
                    <i class="bx bx-envelope"></i>
                </div>
                <div class="input-box">
                    <input type="text" name="Pseudo" class="input-field" placeholder="Pseudo" required>
                    <i class="bx bx-user"></i>
                </div>
                <div class="input-box">
                    <input type="password" name="Password" class="input-field" placeholder="Mot de Passe" required>
                    <i class="bx bx-lock-alt"></i>
                </div>
                <div class="input-box">
                    <select name="Preference" class="input-field" required>
                        <option value="" disabled selected>Choisissez votre préférence</option>
                        <option value="Cerise">Cerise: Trouver sa moitié</option>
                        <option value="Raisin">Raisin: Boire un verre sans se prendre la grappe</option>
                        <option value="Pasteque">Pastèque: Des câlins sans pépins</option>
                        <option value="Peche">Pêche: Un coup d'un soir</option>
                    </select>
                    <i class="bx bx-heart"></i>
                </div>
                <div class="input-box">
                    <input type="submit" class="submit" value="Valider">
                </div>
                <div class="two-col">
                    <div class="one">
                        <input type="checkbox" name="register-check" id="register-check">
                        <label for="register-check">Se souvenir</label>
                    </div>
                    <div class="two">
                        <label><a href="#">Termes & conditions</a></label>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
       function register() {
            var x = document.getElementById("login");
            var y = document.getElementById("register");

            x.style.left = "-510px";
            y.style.right = "5px";
            x.style.opacity = 0;
            y.style.opacity = 1;
       }
    </script>

</body>
</html>
